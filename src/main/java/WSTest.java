import fr.ratp.wsiv.GetLines;
import fr.ratp.wsiv.ObjectFactory;
import fr.ratp.wsiv.Wsiv;
import fr.ratp.wsiv.WsivPortType;
import fr.ratp.wsiv.xsd.Line;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

public class WSTest {
    public static void main(String[] args) {
        WsivPortType ratp = new Wsiv().getWsivSOAP11PortHttp();
        JAXBElement<String> bus139 = new JAXBElement<String>(QName.valueOf("id"), String.class, "B139" );
        Line line = new Line();
        line.setId(bus139);
        ratp.getLines(line);
    }
}
