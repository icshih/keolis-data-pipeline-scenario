import fr.ratp.wsiv.ObjectFactory;
import fr.ratp.wsiv.Wsiv;
import fr.ratp.wsiv.WsivPortType;
import fr.ratp.wsiv.xsd.Line;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import static org.junit.Assert.assertNull;

public class TestWsiv {

    final Logger logger = LoggerFactory.getLogger(TestWsiv.class);

    @Test
    public void test1() {
        WsivPortType ratp = new Wsiv().getWsivSOAP11PortHttp();
        logger.info(ratp.getVersion().toString());
    }

    @Test
    public void test2() {
        ObjectFactory ratp = new ObjectFactory();
        assertNull(ratp.createGetVersionResponse().getReturn());
    }

    @Test
    public void test3() {
        WsivPortType ratp = new Wsiv().getWsivSOAP11PortHttp();
        JAXBElement<String> bus139 = new JAXBElement<>(QName.valueOf("id"), String.class, "B139");
        Line line = new Line();
        line.setId(bus139);
        ratp.getLines(line);
    }
}
