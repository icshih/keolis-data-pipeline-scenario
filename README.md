# Keolis Data Pipeline Scenario

This repository is a collection of java code for the Project Pipeline proposed by Eric. The presentation of the project can be found in _doc/Prsentation-Streaming-ELK-Cassandra.pptx_ .

## Pre-requisites

Be sure to install **Java 8+ SDK** and **Apache Maven** in the local development environment.

## Java 8+

All the code requires Java 8+ because of the usage of [lambda expressions](https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html).

## Maven
The java package management and building system.

To build the executable jar file, simply go to the root directory of the package

    mvn clean package 