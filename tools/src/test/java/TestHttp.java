import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.fluent.Content;
import org.apache.hc.client5.http.fluent.Request;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class TestHttp {

    private final String token = "69de5d8e-6891-4981-8e2c-fe24daeeb774";

    @Test
    public void testHttpClient5AuthBasic() {
        final BasicCredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope("api.sncf.com", 443),
                new UsernamePasswordCredentials(token, null));
        try (final CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .build()) {
            final HttpGet httpget = new HttpGet("https://api.sncf.com/v1/coverage");

            System.out.println("Executing request " + httpget.getMethod() + " " + httpget.getUri());
            try (final CloseableHttpResponse response = httpclient.execute(httpget)) {
                System.out.println("----------------------------------------");
                System.out.println(response.getCode() + " " + response.getReasonPhrase());
                System.out.println(EntityUtils.toString(response.getEntity()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testHttpClient5() {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet("https://" + token + "@api.sncf.com/v1/coverage");
            System.out.println("Executing request " + httpGet.getMethod() + " " + httpGet.getUri());
            try (CloseableHttpResponse response = httpclient.execute(httpGet);) {
                System.out.println("----------------------------------------");
                System.out.println(response.getCode() + " " + response.getReasonPhrase());
                HttpEntity entity = response.getEntity();
                System.out.println(EntityUtils.toString(entity));
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testHttpClinet5Fluent() {
        try {
            Content content = Request.get("https://" + token + "@api.sncf.com/v1/coverage")
                    .execute()
                    .returnContent();
            System.out.println(content.asString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
