package ics.dev.tools;

import org.apache.hc.client5.http.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class RestClient {

    final static Logger logger = LoggerFactory.getLogger(RestClient.class);

    /***
     * Simple HTTP Get
     * @param url
     * @return content of the response in string, otherwise empty string
     */
    public static String get(String url) {
        try {
            return Request.get(url)
                    .execute()
                    .returnContent()
                    .asString();
        } catch (IOException e) {
            logger.error("Connection failed, see ", e);
            return "";
        }
    }
}
