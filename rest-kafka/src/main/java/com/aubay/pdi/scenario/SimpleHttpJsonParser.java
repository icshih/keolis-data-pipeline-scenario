package com.aubay.pdi.scenario;

import com.google.gson.Gson;
import ics.dev.data.model.Station;
import ics.dev.data.model.StationStatus;
import ics.dev.data.model.Stations;
import ics.dev.tools.RestClient;

public class SimpleHttpJsonParser {

    public static void main(String[] args) {
        String stationInfo = RestClient.get("https://velib-metropole-opendata.smoove.pro/opendata/Velib_Metropole/station_information.json");

        Gson gson = new Gson();
        StationStatus ss = gson.fromJson(stationInfo, StationStatus.class);
        System.out.println(ss.getLastUpdatedOther());
        System.out.println(ss.getTtl());
        Stations data = ss.getData();
        data.getStations().stream()
                .filter(s -> s.getName().contains("Botzaris"))
                .map(Station::toString)
                .forEach(System.out::println);

    }
}
