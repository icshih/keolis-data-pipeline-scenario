package com.aubay.pdi.scenario;

import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class SimpleAvroConsumer {

    private final CountDownLatch latch = new CountDownLatch(1);
    private final Logger logger = LoggerFactory.getLogger(SimpleAvroConsumer.class);

    private SimpleAvroConsumer() {}

    private Properties confAvroConsumer() {
        String bootstrapServer = "192.168.64.9:9092";
        String schemaUrl = "http://192.168.64.9:8081";
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "test-station-information");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        props.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaUrl);
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
        return props;
    }

    private void run() {
        logger.info("Create KafkaConsumer");
        Properties props = confAvroConsumer();
        KafkaConsumer<String, velib.avro.Station> consumer = new KafkaConsumer<>(props);
        String topic = "station-information";

        logger.info("Create the consumer thread");
        ConsumerRunner runner = new ConsumerRunner(latch, consumer, topic);
        Thread thread = new Thread(runner);
        thread.start();

        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            runner.shutdown();
            try {
                thread.join(1000);
                logger.info("Consumer thread terminated gracefully.");
            } catch (InterruptedException e) {
                logger.warn("Consumer thread interrupted, see ", e);
            }
        }));
    }

    public class ConsumerRunner implements Runnable {

        private final CountDownLatch latch;
        private final KafkaConsumer<String, velib.avro.Station> consumer;
        private final String topic;

        public ConsumerRunner(CountDownLatch latch,
                              KafkaConsumer<String, velib.avro.Station> consumer,
                              String topic) {
            this.latch = latch;
            this.consumer = consumer;
            this.topic = topic;
        }

        @Override
        public void run() {
            logger.info("Connect to Kafka topic " + topic);
            consumer.subscribe(Collections.singletonList(topic));
            while (latch.getCount() > 0) {
                ConsumerRecords<String, velib.avro.Station> records = consumer.poll(Duration.ofMillis(100));
                records.forEach(r -> {
                       velib.avro.Station station = r.value();
                       System.out.println(String.format("%s %s %d", station.getStationId(), station.getName(), station.getCapacity()));
                });
            }
        }

        public void shutdown() {
            logger.info("Receive shutdown signal...");
            latch.countDown();
            consumer.wakeup();
        }
    }

    public static void main(String[] args) {
        new SimpleAvroConsumer().run();
    }
}
