package com.aubay.pdi.scenario;

import com.google.gson.Gson;
import ics.dev.data.model.Station;
import ics.dev.data.model.StationStatus;
import ics.dev.data.model.Stations;
import ics.dev.tools.RestClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClientConfig;
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Properties;

public class SimpleHttpProducer {

    private Gson gson = new Gson();
    final Logger logger = LoggerFactory.getLogger(SimpleHttpProducer.class);

    private SimpleHttpProducer() {}

    private Properties confAvroProducer() {
        String bootstrapServer = "192.168.64.9:9092";
        String schemaUrl = "http://192.168.64.9:8081";
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        props.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaUrl);
        return props;
    }

    private List<Station> connectVelibApi() {
        String stationInfo = RestClient.get("https://velib-metropole-opendata.smoove.pro/opendata/Velib_Metropole/station_information.json");
        StationStatus ss = gson.fromJson(stationInfo, StationStatus.class);
        logger.info("Last Update: " + ss.getLastUpdatedOther());
        logger.info("TTL:" + ss.getTtl());
        Stations data = ss.getData();
        return data.getStations();
    }

    public void run() {
        logger.info("Config Producer and Schema Registry properties");
        Properties props = confAvroProducer();

        logger.info("Connect to Velib Open Data Api");
        List<Station> stations = connectVelibApi();
        logger.info(stations.size() + " data received.");

        logger.info("Write station data to Kafka topic");
        String topic = "station-information";
        try (KafkaProducer<String, velib.avro.Station> producer = new KafkaProducer<>(props)) {
            stations.stream()
                    .map(s -> new velib.avro.Station(s.getStation_id(), s.getName(), s.getLat(), s.getLon(), s.getCapacity(), s.getStationCode()))
                    .map(s -> new ProducerRecord<>(topic, String.valueOf(s.getStationId()), s))
                    .forEach(producer::send);
            producer.flush();
            logger.info("Messages sent.");
        }
    }

    public static void main(String[] args) {
        new SimpleHttpProducer().run();
    }
}
