package com.aubay.pdi.scenario;

import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Properties;

public class SimpleAvroStream {

    private final Logger logger = LoggerFactory.getLogger(SimpleAvroStream.class);

    private Properties confAvroStream() {
        String bootstrapServer = "192.168.64.9:9092";
        String schemaUrl = "http://192.168.64.9:8081";
        Properties props = new Properties();
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test-streams");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class);
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        props.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaUrl);
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return props;
    }

    private void run() {
        String topic = "station-information";
        logger.info("Create a Kafka streams to Kafka topic " + topic);
        Properties props = confAvroStream();

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, velib.avro.Station> input = builder.stream(topic);
        input.filter((k, v) -> v.getCapacity() >= 10).to("station-capacity-ge-10");
        Topology topology = builder.build();
        System.out.println(topology.describe().toString());
        KafkaStreams kStreams = new KafkaStreams(topology, props);
        kStreams.start();  //Streams is running in different thread

        Runtime.getRuntime().addShutdownHook(new Thread( () -> kStreams.close(Duration.ofMillis(1000L))
        ));
    }

    public static void main(String[] args) {
        new SimpleAvroStream().run();
    }
}
