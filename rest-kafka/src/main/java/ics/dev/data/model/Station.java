package ics.dev.data.model;

public class Station {
    private long station_id;
    private String name;
    private double lat;
    private double lon;
    private int capacity;
    private String stationCode;

    public Station(long station_id, String name, double lat, double lon, int capacity, String stationCode) {
        this.station_id = station_id;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.capacity = capacity;
        this.stationCode = stationCode;
    }

    public long getStation_id() {
        return station_id;
    }

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public int getCapacity() {
        return capacity;
    }

    public String getStationCode() {
        return stationCode;
    }

    @Override
    public String toString() {
        return "Station{" +
                "station_id=" + station_id +
                ", name='" + name + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", capacity=" + capacity +
                ", stationCode='" + stationCode + '\'' +
                '}';
    }
}
