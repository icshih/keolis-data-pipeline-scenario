package ics.dev.data.model;

import java.util.Collection;

public class StationStatus {

    private long lastUpdatedOther;
    private long ttl;
    private Stations data;

    public StationStatus() {}

    public long getLastUpdatedOther() {
        return lastUpdatedOther;
    }

    public long getTtl() {
        return ttl;
    }

    public Stations getData() {
        return data;
    }
}
