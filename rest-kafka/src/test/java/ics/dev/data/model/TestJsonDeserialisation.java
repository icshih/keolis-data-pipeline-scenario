package ics.dev.data.model;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class TestJsonDeserialisation {

    @Test
    public void testParseVelibStation() {
        Gson gson = new Gson();
        try {
            FileReader reader = new FileReader(new File("src/test/resources/station.avsc"));
            StationStatus ss = gson.fromJson(reader, StationStatus.class);
            System.out.println(ss.getLastUpdatedOther());
            System.out.println(ss.getTtl());
            Stations data = ss.getData();
            Assert.assertNotNull(data);
            Assert.assertNotNull(data.getStations());
            data.getStations().stream()
                    .filter(s -> s.getName().contains("Botzaris"))
                    .map(Station::toString)
                    .forEach(System.out::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
