# REST - Kafka Interface

This module provides several demonstration applications including,

1. Kafka Producer accesses the open data via the HTTP REST API then send the refined data to a Kafka topic;

2. Kafka Consumer reads the kafka topic, or
 
3. Kafka Stream processes the data then sending the processed data to another Kafka topic.

We use [Confluent Platform](https://www.confluent.io/product/confluent-platform) Community Edition as it integrates the 
Schema Registry which provides the ability to use [Apache Avro](https://avro.apache.org/) for data Serialization/Deserialization.